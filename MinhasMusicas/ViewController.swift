//
//  ViewController.swift
//  MinhasMusicas
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    struct Musica {
        let nomeMusica : String;
        let nomeAlbum : String;
        let nomeCantor : String;
        let nomeImagemPequena : String;
        let nomeImagemGrande : String;
    }
    
    var listaMusicas : [Musica] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaMusicas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let musica = self.listaMusicas[indexPath.row]
        cell.Musica.text = musica.nomeMusica;
        cell.Album.text = musica.nomeAlbum;
        cell.Cantor.text = musica.nomeCantor;
        cell.Capa.image = UIImage(named: musica.nomeImagemPequena)
        return cell
    }
    

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.listaMusicas.append(Musica(nomeMusica: "Pontos Cardeais", nomeAlbum: "Album vivo", nomeCantor: "Alceu Valenca", nomeImagemPequena: "capa_alceu_pequeno", nomeImagemGrande: "capa_alceu_grande"))
        self.listaMusicas.append(Musica(nomeMusica: "Menor abandonado", nomeAlbum: "Album Patota de Cosme", nomeCantor: "Zeca Pagodinho", nomeImagemPequena: "capa_zeca_pequeno", nomeImagemGrande: "capa_zeca_grande"))
        self.listaMusicas.append(Musica(nomeMusica: "Tiro ao Alvaro", nomeAlbum: "Album Adoniran Barbosa e Convidados", nomeCantor: "Adorian Barbosa", nomeImagemPequena: "capa_adoniran_pequeno", nomeImagemGrande: "capa_adhoniran_grande"))
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detalhesViewController = segue.destination as! DetalheMusicaViewController
        let indice = sender as! Int
        let musica = self.listaMusicas[indice]
        detalhesViewController.nomeImage = musica.nomeImagemGrande
        detalhesViewController.nomeAlbum = musica.nomeAlbum
        detalhesViewController.nomeCantor = musica.nomeCantor
        detalhesViewController.nomeMusica = musica.nomeMusica
    }
}

